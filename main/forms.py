from django import forms 
from .models import Model 
  
  
# creating a form 
class Form(forms.ModelForm): 
  
    # create meta class 
    class Meta: 
        # specify model to be used 
        model = Model 
  
        # specify fields to be used 
        fields = [ 
            "nama_Matkul", 
            "dosen_Pengajar", 
            "jumlah_SKS",
            "semester_Tahun",
            "description",
        ] 

        
from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('resume/', views.resume, name='resume'),
    path('result/', views.result, name='result'),
    path('add/', views.add, name='add'),
    path('detail/', views.detail, name='detail'),
    path('update/', views.updated_detail, name='updated_detail'),
    path('delete/', views.delete, name='delete'),
]
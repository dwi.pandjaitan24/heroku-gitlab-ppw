from django.shortcuts import render
from .forms import Form
from .models import Model
from django.shortcuts import (get_object_or_404, render, HttpResponseRedirect)

def home(request):
    return render(request, 'main/home.html')

def about(request):
    return render(request, 'main/about.html')
    
def resume(request):
    return render(request, 'main/resume.html')

def add(request): 
    context ={} 
   
    form = Form(request.POST or None) 
    if form.is_valid(): 
        form.save() 
          
    context['form']= form 
    return render(request, "main/add.html", context)

def result(request):  
    context ={} 
  
    context["dataset"] = Model.objects.all() 
          
    return render(request, "main/result.html", context) 

def detail(request, id):  
    context ={} 

    context["data"] = Model.objects.get(id = id) 
           
    return render(request, "main/detail.html", context) 
   
def updated_detail(request, id): 
    context ={} 
   
    obj = get_object_or_404(Model, id = id) 
  
    form = Form(request.POST or None, instance = obj) 
  
    # save the data from the form and 
    # redirect to detail
    if form.is_valid(): 
        form.save() 
        return HttpResponseRedirect("/"+id) 

    context["form"] = form 
  
    return render(request, "updated_detail.html", context)

def delete(request, id):  
    context ={} 
  
    obj = get_object_or_404(Model, id = id) 
  
  
    if request.method =="POST":  
        obj.delete() 
        # after deleting redirect to  
        # home page 
        return HttpResponseRedirect("/") 
  
    return render(request, "delete.html", context) 
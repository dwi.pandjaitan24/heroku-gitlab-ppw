from django.db import models

# Create your models here.
class Model(models.Model): 
  
    # fields of the model 
    nama_Matkul = models.CharField(max_length=60)
    dosen_Pengajar = models.CharField(max_length=60)
    jumlah_SKS = models.CharField(max_length=5)
    semester_Tahun = models.CharField(max_length=60)
    description = models.TextField()
   
    def __str__(self): 
        return self.nama_Matkul
    
     
from django import forms 
from .models import Model 
  
  
# creating a form 
class Form(forms.ModelForm): 
  
    # create meta class 
    class Meta: 
        # specify model to be used 
        model = Model 
  
        # specify fields to be used 
        fields = [ 
            "title", 
            "description", 
        ] 